# Receta de tortilla de patatas

![Imagen de tortilla](/tortilla-española.jpg)

Típica receta de tortilla a la española

**Ingredientes**

- 4 Huevos
- 400g de Patatas
- 1 Cebolla
- Sal
- Aceite de oliva virgen extra

**Pasos a seguir**

1. Pelamos y lavamos las patatas, las cortamos en rodajas igual que la cebolla, ponemos las dos en una sartén con aceite.
2. Después de un tiempo de cocción, las sacamos de la sartén y las ponemos en un cuenco grande. Batimos los huevos y los añadimos a las patatas y a la cebolla, añadimos un poco de sal y mezclamos.
3. Ponemos en la sartén un par de cucharadas de aceite y vertemos todo. Al principio rompemos un poco, y vamos dándole forma por los bordes. Cuando veamos que ya está cuajada por abajo, ponemos un plato y le damos la vuelta rápidamente.
4. Dejamos unos minutos más para que se termine de hacer. Y ya estará lista nuestra tortilla de patatas.

**Referencia**

[https://www.pequerecetas.com/receta/tortilla-de-patatas/](Enllaç a pàgina web de referencia)
